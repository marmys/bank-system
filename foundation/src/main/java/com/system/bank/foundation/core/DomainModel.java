package com.system.bank.foundation.core;

public abstract class DomainModel<E extends DomainEntity> {

    protected final E entity;

    protected DomainModel(E entity) {
        this.entity = entity;
    }

    public E unwrap() {
        return entity;
    }
}
