package com.system.bank.foundation.core;

import org.springframework.data.repository.CrudRepository;

public abstract class DomainRepository<M extends DomainModel<E>, E extends DomainEntity> {

    protected final CrudRepository<E, Long> dao;

    protected DomainRepository(CrudRepository<E, Long> dao) {
        this.dao = dao;
    }

    protected M save(M domainModel) {
        return wrap(dao.save(domainModel.unwrap()));
    }

    protected abstract M wrap(E entity);

}
