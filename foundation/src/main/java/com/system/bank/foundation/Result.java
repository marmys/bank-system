package com.system.bank.foundation;

import lombok.Getter;

@Getter
public class Result {
    private static final Result SUCCESS_RESULT = new Result("Operation finished successfully", ResultType.SUCCESS);

    private final String message;
    private final ResultType type;

    private Result(String message, ResultType type) {
        this.message = message;
        this.type = type;
    }

    public static Result createFailureResponse(String message) {
        return new Result(message, ResultType.FAIL);
    }

    public static Result createSuccessResponse() {
        return SUCCESS_RESULT;
    }

    public enum ResultType {
        SUCCESS,
        FAIL
    }
}
