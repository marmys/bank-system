package com.system.bank.user.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class UserConfiguration {

    @Bean
    UserRepository userDomainRepository(UserDao userDao) {
        return new UserRepository(userDao, getPasswordEncoder());
    }

    @Bean
    UserService userService(UserRepository repository) {
        return new UserService(repository);
    }

    private PasswordEncoder getPasswordEncoder() {
        return password -> password;
    }
}
