package com.system.bank.user.core;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface UserDao extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findByLogin(String login);
}
