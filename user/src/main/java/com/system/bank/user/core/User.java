package com.system.bank.user.core;

import com.system.bank.foundation.core.DomainModel;
import com.system.bank.user.api.UserDto;

class User extends DomainModel<UserEntity> {

    private final PasswordEncoder passwordEncoder;

    User(UserEntity entity, PasswordEncoder passwordEncoder) {
        super(entity);
        this.passwordEncoder = passwordEncoder;
    }

    User changePassword(String password) {
        entity.setPassword(passwordEncoder.encode(password));
        return this;
    }

    User updateDetails(UserDto dto) {
        entity.setDateOfBirth(dto.getDateOfBirth());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setEmail(dto.getEmailAddress());
        return this;
    }

}
