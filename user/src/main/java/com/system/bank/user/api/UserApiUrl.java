package com.system.bank.user.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserApiUrl {

    public static final String BASE = "/user";
}
