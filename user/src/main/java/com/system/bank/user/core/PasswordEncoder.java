package com.system.bank.user.core;

interface PasswordEncoder {
    String encode(String password);
}
