package com.system.bank.user.core;

import com.system.bank.foundation.Result;
import com.system.bank.user.api.UserDto;
import org.springframework.transaction.annotation.Transactional;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public Result createNew(UserDto dto) {
        if (isLoginUnique(dto.getLogin())) {
            User user = userRepository.createNew(dto.getLogin())
                    .changePassword(dto.getPassword())
                    .updateDetails(dto);
            userRepository.save(user);
            return Result.createSuccessResponse();
        } else {
            return Result.createFailureResponse("User with this login already exists.");
        }
    }

    private boolean isLoginUnique(String login) {
        return !userRepository.findByLogin(login).isPresent();
    }

}
