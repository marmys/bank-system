package com.system.bank.user.core;

import com.system.bank.foundation.core.DomainRepository;

import java.util.Optional;

class UserRepository extends DomainRepository<User, UserEntity> {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;

    UserRepository(UserDao userDao, PasswordEncoder passwordEncoder) {
        super(userDao);
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    User createNew(String login) {
        UserEntity entity = new UserEntity();
        entity.setLogin(login);
        return wrap(entity);
    }

    @Override
    protected User save(User domainModel) {
        return super.save(domainModel);
    }

    Optional<User> findByLogin(String login) {
        return userDao.findByLogin(login).map(this::wrap);
    }

    @Override
    protected User wrap(UserEntity entity) {
        return new User(entity, passwordEncoder);
    }
}
