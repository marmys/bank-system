package com.system.bank.user.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserDto {
    private String login;
    private String password;
    private String phoneNumber;
    private String emailAddress;
    private LocalDate dateOfBirth;
}
