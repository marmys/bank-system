package com.system.bank.user.core

import com.system.bank.foundation.Result
import com.system.bank.user.api.UserDto
import spock.lang.Specification
import spock.lang.Unroll

import static com.system.bank.foundation.Result.ResultType.FAIL
import static com.system.bank.foundation.Result.ResultType.SUCCESS

class UserServiceTest extends Specification {

    final static String USER_LOGIN = 'Test user login'
    final static String PASSWORD = 'TestPassword'
    final static String PHONE_NUMBER = '000111222'

    UserService userService
    UserDao userDaoMock

    @Unroll
    def 'should return #resultType on user creation'() {
        given: 'data for new user'
            UserDto userDto = new UserDto(login: USER_LOGIN, password: PASSWORD, phoneNumber: PHONE_NUMBER)
        and: 'user with the same login in database'
            userDaoMock.findByLogin(_ as String) >> Optional.ofNullable(foundByLogin)

        when:
            Result result = userService.createNew(userDto)

        then: 'user creation result'
            result.type == resultType
        and: 'user entity save method called'
            saveMethodCalls * userDaoMock.save(_ as UserEntity)

        where:
            foundByLogin                      || resultType | saveMethodCalls
            null                              || SUCCESS    | 1
            new UserEntity(login: USER_LOGIN) || FAIL       | 0
    }

    def setup() {
        userDaoMock = Mock(UserDao)
        def configuration = new UserConfiguration()
        userService = configuration.userService(configuration.userDomainRepository(userDaoMock))
    }
}
