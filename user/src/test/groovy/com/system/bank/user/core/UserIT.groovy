package com.system.bank.user.core

import com.fasterxml.jackson.databind.ObjectMapper
import com.system.bank.user.TestContext
import com.system.bank.user.api.UserApiUrl
import com.system.bank.user.api.UserDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@ContextConfiguration(classes = TestContext.class)
@SpringBootTest(properties = "test.properties", webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class UserIT extends Specification {

    final String USER_LOGIN = 'Test user login'
    final String PASSWORD = 'TestPassword'
    final String PHONE_NUMBER = '000111222'

    @Autowired
    MockMvc mockMvc

    @Autowired
    UserDao userDao

    def 'should create new user'() {
        given: 'data for new user'
            UserDto userDto = new UserDto(login: USER_LOGIN, password: PASSWORD, phoneNumber: PHONE_NUMBER)

        when: 'create a user request sent'
            int statusCode = httpCreateUserRequest(userDto).getStatus()

        then: 'request handled correctly'
            statusCode == HttpStatus.OK.value()
        and: 'a new user stored in database'
            UserEntity userEntity = findAllUsers().find()
            userEntity.login == USER_LOGIN
    }

    MockHttpServletResponse httpCreateUserRequest(UserDto userDto) {
        String jsonData = new ObjectMapper().writeValueAsString(userDto)
        mockMvc.perform(post(UserApiUrl.BASE)
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonData))
                .andReturn().getResponse()
    }

    List<UserEntity> findAllUsers() {
        userDao.findAll().toList()
    }
}