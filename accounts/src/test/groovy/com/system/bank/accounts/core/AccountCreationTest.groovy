package com.system.bank.accounts.core

import com.system.bank.accounts.api.AccountDto
import com.system.bank.accounts.api.AccountType
import com.system.bank.foundation.Result
import org.apache.commons.lang3.StringUtils
import spock.lang.Specification

class AccountCreationTest extends Specification implements AccountTrait {

    def 'should create new account for the user'() {
        given: 'user ID'
            Long userId = createIdValue()
        and: 'new account details'
            AccountDto accountDto = new AccountDto(type: AccountType.STANDARD)

        when:
            Result result = accountService.createNew(userId, accountDto)

        then: 'account creation finished with success'
            result.type == Result.ResultType.SUCCESS
        and: 'new account has been saved'
            1 * accountDao.save(_ as AccountEntity)
    }


    def 'should throw exception on missing user ID'() {
        given: 'account details'
            AccountDto accountDto = new AccountDto(type: AccountType.STANDARD)
        when:
            accountService.createNew(null, accountDto)

        then: 'exception should be thrown'
            Exception e = thrown(RuntimeException)
        and: 'message is provided'
            StringUtils.isNotBlank(e.message)
        and: 'new account not saved'
            0 * accountDao.save(_ as AccountEntity)
    }

    def 'should throw exception on missing account type'() {
        given: 'user ID'
            Long userId = createIdValue()
        and: 'account data without type'
            AccountDto accountDto = new AccountDto(type: null)
        when:
            accountService.createNew(userId, accountDto)

        then: 'exception should be thrown'
            Exception e = thrown(RuntimeException)
        and: 'message is provided'
            StringUtils.isNotBlank(e.message)
        and: 'new account not saved'
            0 * accountDao.save(_ as AccountEntity)
    }

    def setup() {
        accountDao = Mock(AccountDao)
        createAccountService(accountDao)
    }

    long createIdValue() {
        new Random().nextLong() % 100000
    }
}
