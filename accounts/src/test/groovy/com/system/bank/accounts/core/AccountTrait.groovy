package com.system.bank.accounts.core

import groovy.transform.CompileStatic

@CompileStatic
trait AccountTrait {

    AccountService accountService
    AccountDao accountDao

    def createAccountService(AccountDao accountDao) {
        AccountConfiguration configuration = new AccountConfiguration()
        accountService = configuration.accountService(configuration.accountRepository(accountDao))
    }
}
