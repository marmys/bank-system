package com.system.bank.accounts.core

import com.fasterxml.jackson.databind.ObjectMapper
import com.system.bank.accounts.TestContext
import com.system.bank.accounts.api.AccountDto
import com.system.bank.accounts.api.AccountType
import com.system.bank.accounts.api.ApiUrl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import spock.lang.Specification

import java.math.RoundingMode

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@ContextConfiguration(classes = TestContext)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class AccountIT extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    AccountDao accountDao

    def 'should create new account for the user'() {
        given: 'user ID'
            long userId = 1L
        and: 'new account details'
            AccountDto accountDto = new AccountDto(type: AccountType.STANDARD)

        when:
            httpCreateAccountRequest(userId, accountDto)

        then: 'account created'
            AccountEntity accountEntity = findAllAccounts().find()
            accountEntity.userId == userId
    }

    def 'should add value to new account'() {
        given: 'user ID'
            long accountId = createNewAccount()
        and: 'value to add'
            BigDecimal value = new BigDecimal(10.59)

        when:
            httpAddValueToBalanceAccountRequest(accountId, value)

        then: 'balance set with scale'
            AccountEntity accountEntity = findAllAccounts().find()
            accountEntity.balance == value.setScale(2, RoundingMode.HALF_UP)
    }

    MockHttpServletResponse httpCreateAccountRequest(long userId, AccountDto accountDto) {
        sendRequest(post(ApiUrl.USER + "/" + userId + ApiUrl.ACCOUNT), accountDto)
    }

    MockHttpServletResponse httpAddValueToBalanceAccountRequest(long accountId, BigDecimal value) {
        sendRequest(patch(ApiUrl.USER + "/1" + ApiUrl.ACCOUNT + "/" + accountId), value)
    }

    private MockHttpServletResponse sendRequest(MockHttpServletRequestBuilder httpMethodMapping, Object data) {
        String jsonData = new ObjectMapper().writeValueAsString(data)
        mockMvc.perform(httpMethodMapping
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonData))
                .andReturn().getResponse()
    }

    private long createNewAccount() {
        httpCreateAccountRequest(1L, new AccountDto(type: AccountType.STANDARD))
        accountDao.findAll().toList()[0].id
    }

    List<AccountEntity> findAllAccounts() {
        accountDao.findAll().toList()
    }
}
