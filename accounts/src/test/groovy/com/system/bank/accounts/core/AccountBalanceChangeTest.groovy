package com.system.bank.accounts.core

import com.system.bank.foundation.Result
import spock.lang.Specification
import spock.lang.Unroll

import java.math.RoundingMode

import static com.system.bank.foundation.Result.ResultType.FAIL
import static com.system.bank.foundation.Result.ResultType.SUCCESS

class AccountBalanceChangeTest extends Specification implements AccountTrait {

    @Unroll
    def 'add #amount to account with #initialBalance balance'() {
        given: 'an account with'
            AccountEntity account = createAccount(initialBalance)
        and: 'value to be added to account'
            BigDecimal value = new BigDecimal(amount)

        when:
            accountService.addValue(account.id, value)

        then: 'current value is sum of initialBalance and amount'
            account.balance == expectedBalance

        where:
            initialBalance | amount || expectedBalance
            0.0            | 125.65 || 125.65
            15.95          | 130.12 || 146.07
            -5.95          | 2.01   || -3.94
    }

    @Unroll
    def 'reduce #initialBalance balance by #amount'() {
        given: 'an account with'
            AccountEntity account = createAccount(initialBalance)
        and: 'value to be added to account'
            BigDecimal value = new BigDecimal(amount)

        when:
            Result result = accountService.reduceBalance(account.id, value)

        then:
            result.type == SUCCESS
        and:
            account.balance == expectedBalance

        where:
            initialBalance | amount || expectedBalance
            125.65         | 125.65 || 0.0
            146.07         | 130.12 || 15.95
    }

    def 'balance cannot be reduced if smaller than value'() {
        given: 'initialBalance'
            double initialBalance = 0.0
        and: 'an account with'
            AccountEntity account = createAccount(initialBalance)
        and: 'value to be added to account'
            BigDecimal value = new BigDecimal(initialBalance + 0.01)

        when:
            Result result = accountService.reduceBalance(account.id, value)

        then:
            result.type == FAIL
        and:
            account.balance == new BigDecimal(initialBalance).setScale(2, RoundingMode.HALF_UP)
    }

    def 'value to add should be positive'() {
        given: 'an account with'
            AccountEntity account = createAccount(0.0)
        and: 'negative value to be added to account'
            BigDecimal value = new BigDecimal(-0.1)

        when:
            accountService.addValue(account.id, value)

        then: 'runtime exception thrown to break transaction'
            thrown(RuntimeException)
    }

    def 'value to reduce balance should be positive'() {
        given: 'an account with'
            AccountEntity account = createAccount(0.0)
        and: 'negative value to reduce balance'
            BigDecimal value = new BigDecimal(-0.1)

        when:
            accountService.reduceBalance(account.id, value)

        then: 'runtime exception thrown to break transaction'
            thrown(RuntimeException)
    }

    def setup() {
        accountDao = Mock(AccountDao)
        createAccountService(accountDao)
    }

    AccountEntity createAccount(double balance) {
        AccountEntity entity = new AccountEntity(id: 1L, balance: new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP))
        accountDao.findById(entity.id) >> Optional.of(entity)
        entity
    }
}
