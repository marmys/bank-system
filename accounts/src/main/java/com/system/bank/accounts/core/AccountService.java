package com.system.bank.accounts.core;

import com.system.bank.accounts.api.AccountDto;
import com.system.bank.foundation.Result;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Transactional
public class AccountService {

    private final AccountRepository accountRepository;

    AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Result createNew(Long ownerId, AccountDto dto) {
        accountRepository.save(accountRepository.createNew(ownerId, dto.getType()));
        return Result.createSuccessResponse();
    }

    public Result addValue(long accountId, BigDecimal value) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isPresent()) {
            accountOptional.get().addToBalance(value);
            return Result.createSuccessResponse();
        } else {
            return Result.createFailureResponse("This account does not exist");
        }
    }

    public Result reduceBalance(long accountId, BigDecimal value) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (!accountOptional.isPresent()) {
            return Result.createFailureResponse("This account does not exist");
        }
        Account account = accountOptional.get();
        if (!account.canBeReducedWith(value)) {
            return Result.createFailureResponse(String.format("Current account balance cannot be reduced with %s", value.setScale(2, RoundingMode.HALF_UP)));
        }
        account.reduceBalance(value);
        return Result.createSuccessResponse();
    }

}
