package com.system.bank.accounts.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class AccountConfiguration {

    @Bean
    AccountRepository accountRepository(AccountDao dao) {
        return new AccountRepository(dao);
    }

    @Bean
    AccountService accountService(AccountRepository repository) {
        return new AccountService(repository);
    }
}
