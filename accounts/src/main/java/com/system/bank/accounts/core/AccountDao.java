package com.system.bank.accounts.core;

import org.springframework.data.repository.CrudRepository;

interface AccountDao extends CrudRepository<AccountEntity, Long> {

}
