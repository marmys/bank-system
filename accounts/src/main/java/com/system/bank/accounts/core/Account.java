package com.system.bank.accounts.core;

import com.system.bank.foundation.core.DomainModel;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.google.common.base.Preconditions.checkArgument;

class Account extends DomainModel<AccountEntity> {

    private static final int BALANCE_SCALE = 2;

    protected Account(AccountEntity entity) {
        super(entity);
    }

    void addToBalance(BigDecimal value) {
        checkPositive(value);
        setNewBalance(entity.getBalance().add(scale(value)));
    }

    boolean canBeReducedWith(BigDecimal value) {
        return entity.getBalance().compareTo(scale(value)) >= 0;
    }

    void reduceBalance(BigDecimal value) {
        checkPositive(value);
        setNewBalance(entity.getBalance().subtract(scale(value)));
    }

    private void setNewBalance(BigDecimal newBalanceValue) {
        entity.setBalance(scale(newBalanceValue));
    }

    private BigDecimal scale(BigDecimal value) {
        return value.setScale(BALANCE_SCALE, RoundingMode.HALF_UP);
    }

    private void checkPositive(BigDecimal value) {
        checkArgument(isPositive(value), "Value cannot be negative");
    }

    private boolean isPositive(BigDecimal value) {
        return BigDecimal.ZERO.compareTo(value) < 0;
    }
}
