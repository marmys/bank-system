package com.system.bank.accounts.core;

import com.system.bank.accounts.api.AccountType;
import com.system.bank.foundation.core.DomainRepository;

import java.math.BigDecimal;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

class AccountRepository extends DomainRepository<Account, AccountEntity> {


    protected AccountRepository(AccountDao dao) {
        super(dao);
    }

    Account createNew(Long ownerId, AccountType type) {
        checkNotNull(ownerId, "Cannot create account without owner");
        checkNotNull(type, "Cannot create account without type");
        AccountEntity entity = new AccountEntity();
        entity.setUserId(ownerId);
        entity.setAccountType(type);
        entity.setBalance(BigDecimal.ZERO);
        return wrap(entity);
    }

    @Override
    protected Account save(Account domainModel) {
        return super.save(domainModel);
    }

    @Override
    protected Account wrap(AccountEntity entity) {
        return new Account(entity);
    }

    Optional<Account> findById(long accountId) {
        return this.dao.findById(accountId).map(this::wrap);
    }
}
