package com.system.bank.accounts.api;

import lombok.Data;

@Data
public class AccountDto {
    private Long id;
    private AccountType type;
}
