package com.system.bank.accounts.core;

import com.system.bank.accounts.api.AccountType;
import com.system.bank.foundation.core.DomainEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
@Getter
@Setter(AccessLevel.PACKAGE)
class AccountEntity implements DomainEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Column(nullable = false, scale = 2)
    private BigDecimal balance;

}
