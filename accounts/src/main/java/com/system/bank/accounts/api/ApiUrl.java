package com.system.bank.accounts.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ApiUrl {
    public static final String USER = "/user";
    public static final String ACCOUNT = "/account";
}
