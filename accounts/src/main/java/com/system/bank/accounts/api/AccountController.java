package com.system.bank.accounts.api;

import com.system.bank.accounts.core.AccountService;
import com.system.bank.foundation.Result;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = ApiUrl.USER + "/{userId}" + ApiUrl.ACCOUNT)
    public Result createNewUser(@PathVariable Long userId, @RequestBody AccountDto accountDto) {
        return accountService.createNew(userId, accountDto);
    }

    @PatchMapping(value = ApiUrl.USER + "/{userId}" + ApiUrl.ACCOUNT + "/{id}")
    public Result addValue(@PathVariable Long id, @RequestBody BigDecimal value) {
        return accountService.addValue(id, value);
    }
}
