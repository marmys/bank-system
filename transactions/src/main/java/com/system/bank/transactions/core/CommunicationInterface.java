package com.system.bank.transactions.core;

import com.system.bank.foundation.Result;

import java.math.BigDecimal;

class CommunicationInterface {

    Result sendTransferFromRequest(Long accountId, BigDecimal value) {
        //TODO implement real communication
        throw new UnsupportedOperationException("Not implemented yet");
    }

    Result sendTransferToRequest(Long accountId, BigDecimal value) {
        //TODO implement real communication
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
