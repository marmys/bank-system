package com.system.bank.transactions.core;

import com.system.bank.transactions.api.TransactionsDto;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

class TransactionDetailsAssembler {

    TransactionsDto assemble(Collection<String> detailsNames, Collection<TransactionEntity> entities) {
        Set<TransactionDetail> details = TransactionDetail.findDetailsByNames(detailsNames);
        return new TransactionsDto(entities.stream().map(entity -> {
            Map<String, Object> detailsMap = new HashMap<>(details.size());
            details.forEach(detail -> detailsMap.put(detail.name(), detail.getDetailProvider().apply(entity)));
            return detailsMap;
        }).collect(Collectors.toList()));
    }
}
