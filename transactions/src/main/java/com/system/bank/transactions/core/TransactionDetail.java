package com.system.bank.transactions.core;

import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

@RequiredArgsConstructor
@Getter(AccessLevel.PACKAGE)
enum TransactionDetail {
    ACCOUNT_FROM(TransactionEntity::getAccountFromId),
    ACCOUNT_TO(TransactionEntity::getAccountToId),
    VALUE(TransactionEntity::getValue),
    TIMESTAMP(TransactionEntity::getTimestamp),
    DESCRIPTION(TransactionEntity::getDescription);

    private final Function<TransactionEntity, Object> detailProvider;

    static Set<TransactionDetail> findDetailsByNames(Collection<String> detailNames) {
        return detailNames.stream().map(
                name -> Sets.newHashSet(TransactionDetail.values())
                        .stream()
                        .filter(value -> equalsIgnoreCase(trimToEmpty(name), value.name()))
                        .findFirst().orElse(null))
                .collect(Collectors.toSet());
    }
}
