package com.system.bank.transactions.api;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TransactionDto {
    private Long id;
    private Long accountFromId;
    private Long accountToId;
    private BigDecimal value;
    private LocalDateTime timestamp;
    private String description;
}
