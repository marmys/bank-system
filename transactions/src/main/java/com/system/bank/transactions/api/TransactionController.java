package com.system.bank.transactions.api;

import com.system.bank.foundation.Result;
import com.system.bank.transactions.core.TransactionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransactionController {
    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping(value = ApiUrl.USER + "/{userId}" + ApiUrl.TRANSACTION)
    public TransactionsDto getTransactions(@PathVariable Long userId, @RequestAttribute List<String> details) {
        return transactionService.findTransactions(details);
    }

    @PostMapping(value = ApiUrl.USER + "/{userId}" + ApiUrl.TRANSACTION)
    public Result createNewUser(@PathVariable Long userId, @RequestBody TransactionDto transactionDto) {
        return transactionService.createNew(transactionDto);
    }
}
