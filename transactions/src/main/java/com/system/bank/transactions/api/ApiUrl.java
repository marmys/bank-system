package com.system.bank.transactions.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ApiUrl {
    public static final String USER = "/user";
    public static final String ACCOUNT = "/account";
    public static final String ACCOUNT_FROM = "/account_from";
    public static final String ACCOUNT_TO = "/account_to";
    public static final String TRANSACTION = "/transaction";
}
