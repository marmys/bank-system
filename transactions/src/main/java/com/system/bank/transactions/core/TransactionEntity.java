package com.system.bank.transactions.core;

import com.system.bank.foundation.core.DomainEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter(AccessLevel.PACKAGE)
class TransactionEntity implements DomainEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long accountFromId;

    @Column(nullable = false)
    private Long accountToId;

    @Column(nullable = false, scale = 2)
    private BigDecimal value;

    @Column(nullable = false)
    private LocalDateTime timestamp;

    @Column
    private String description;

    @Override
    public Long getId() {
        return id;
    }
}
