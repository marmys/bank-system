package com.system.bank.transactions.core;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class TransactionConfiguration {

    @Bean
    TransactionService transactionService(TransactionDao dao) {
        return new TransactionService(
                new CommunicationInterface(),
                new TransactionRepository(dao),
                new TransactionDetailsAssembler(),
                dao);
    }
}
