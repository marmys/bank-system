package com.system.bank.transactions.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class TransactionsDto {
    private final List<Map<String, ?>> data;
}
