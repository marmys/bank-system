package com.system.bank.transactions.core;

import com.google.common.collect.Lists;
import com.system.bank.foundation.Result;
import com.system.bank.transactions.api.TransactionDto;
import com.system.bank.transactions.api.TransactionsDto;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;

public class TransactionService {

    private static final int SCALE = 2;
    private final CommunicationInterface communicationInterface;
    private final TransactionRepository transactionRepository;
    private final TransactionDetailsAssembler transactionDetailsAssembler;
    private final TransactionDao transactionDao;

    public TransactionService(CommunicationInterface communicationInterface, TransactionRepository transactionRepository, TransactionDetailsAssembler transactionDetailsAssembler, TransactionDao transactionDao) {
        this.communicationInterface = communicationInterface;
        this.transactionRepository = transactionRepository;
        this.transactionDetailsAssembler = transactionDetailsAssembler;
        this.transactionDao = transactionDao;
    }

    @Transactional
    public Result createNew(TransactionDto transactionDto) {
        BigDecimal valueForTransfer = scaleValue(transactionDto);
        communicationInterface.sendTransferFromRequest(transactionDto.getAccountFromId(), valueForTransfer);
        communicationInterface.sendTransferToRequest(transactionDto.getAccountToId(), valueForTransfer);

        Transaction transaction = transactionRepository.createNew(
                transactionDto.getAccountFromId(),
                transactionDto.getAccountToId(),
                transactionDto.getDescription());

        transactionRepository.save(transaction);
        return Result.createSuccessResponse();
    }

    public TransactionsDto findTransactions(Collection<String> details) {
        return transactionDetailsAssembler.assemble(details, Lists.newArrayList(transactionDao.findAll()));
    }

    private BigDecimal scaleValue(TransactionDto transactionDto) {
        return transactionDto.getValue().setScale(SCALE, RoundingMode.HALF_UP);
    }
}
