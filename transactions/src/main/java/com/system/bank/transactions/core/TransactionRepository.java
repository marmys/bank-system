package com.system.bank.transactions.core;

import com.system.bank.foundation.core.DomainRepository;

import java.time.LocalDateTime;

class TransactionRepository extends DomainRepository<Transaction, TransactionEntity> {
    protected TransactionRepository(TransactionDao dao) {
        super(dao);
    }

    Transaction createNew(Long accountFromId, Long accountToId, String description) {
        TransactionEntity entity = new TransactionEntity();
        entity.setAccountFromId(accountFromId);
        entity.setAccountToId(accountToId);
        entity.setDescription(description);
        entity.setTimestamp(LocalDateTime.now());
        return wrap(entity);
    }

    @Override
    protected Transaction save(Transaction domainModel) {
        return super.save(domainModel);
    }

    @Override
    protected Transaction wrap(TransactionEntity entity) {
        return new Transaction(entity);
    }
}
