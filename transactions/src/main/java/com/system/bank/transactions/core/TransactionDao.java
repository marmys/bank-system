package com.system.bank.transactions.core;

import org.springframework.data.repository.CrudRepository;

interface TransactionDao extends CrudRepository<TransactionEntity, Long> {
}
