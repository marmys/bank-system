package com.system.bank.transactions.core

import com.system.bank.transactions.api.TransactionDto
import spock.lang.Shared
import spock.lang.Specification

class NewTransactionTest extends Specification {
    @Shared
    private long accountFromId

    @Shared
    private long accountToId

    TransactionDao transactionDao
    CommunicationInterface servicesInterface
    TransactionService transactionService

    def 'should create new transaction'() {
        given: 'value to transfer'
            BigDecimal value = new BigDecimal(123.45)
        and: 'accounts for transfer'
            mockAccounts()

        when:
            transactionService.createNew(new TransactionDto(accountFromId: this.accountFromId, accountToId: this.accountToId, value: value))

        then: 'transaction save to database called'
            1 * transactionDao.(_ as TransactionEntity)
    }

    def mockAccounts() {
        accountFromId = 1L
        accountToId = 2L
    }
}
