package com.system.bank.transactions.core


import com.system.bank.transactions.api.TransactionsDto
import spock.lang.Specification

import java.time.LocalDateTime

class GetTransactionsDetailsTest extends Specification {

    TransactionDao transactionDao
    TransactionService transactionService

    def 'should return requested from accounts'() {
        given: 'some transactions'
            mockTransactions()

        when:
            TransactionsDto transactionsDto = transactionService.findTransactions([TransactionDetail.ACCOUNT_FROM.name()])

        then: '4 mocked transactions mapped'
            transactionsDto.data.size() == 4
        and: 'account from parameter is provided'
            transactionsDto.data.each { assert it.containsKey(TransactionDetail.ACCOUNT_FROM.name()) }
        and: 'other parameters not added'
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.ACCOUNT_TO.name()) }
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.VALUE.name()) }
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.DESCRIPTION.name()) }
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.TIMESTAMP.name()) }
    }

    def 'should return requested accounts and value'() {
        given: 'some transactions'
            mockTransactions()

        when:
            TransactionsDto transactionsDto = transactionService.findTransactions([TransactionDetail.ACCOUNT_TO.name(), TransactionDetail.VALUE.name()])

        then: '4 mocked transactions mapped'
            transactionsDto.data.size() == 4
        and: 'account from parameter is provided'
            transactionsDto.data.each { assert it.containsKey(TransactionDetail.ACCOUNT_TO.name()) }
            transactionsDto.data.each { assert it.containsKey(TransactionDetail.VALUE.name()) }
        and: 'other parameters not added'
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.ACCOUNT_FROM.name()) }
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.DESCRIPTION.name()) }
            transactionsDto.data.each { assert !it.containsKey(TransactionDetail.TIMESTAMP.name()) }
    }

    def setup() {
        transactionDao = Mock(TransactionDao)
        transactionService = new TransactionConfiguration().transactionService(transactionDao)
    }

    private def mockTransactions() {
        transactionDao.findAll() >>
                [new TransactionEntity(accountFromId: 1, accountToId: 2, value: new BigDecimal(123.45), timestamp: LocalDateTime.now(), description: 'Test description'),
                 new TransactionEntity(accountFromId: 2, accountToId: 3, value: new BigDecimal(456.78), timestamp: LocalDateTime.now(), description: 'Test description2'),
                 new TransactionEntity(accountFromId: 4, accountToId: 25, value: new BigDecimal(789.09), timestamp: LocalDateTime.now(), description: 'Test description3'),
                 new TransactionEntity(accountFromId: 18, accountToId: 120, value: new BigDecimal(1234.89), timestamp: LocalDateTime.now(), description: 'Test description4'),
                ]
    }
}
