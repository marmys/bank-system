package com.system.bank.transactions

import groovy.transform.CompileStatic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.annotation.EnableTransactionManagement

import javax.sql.DataSource

@Configuration
@ComponentScan(basePackages = "com.system.bank.transactions")
@EnableTransactionManagement
@EnableJpaRepositories
@CompileStatic
class TestContext {

    @Bean
    DataSource dataSource() {
        new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build()
    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, Properties properties) {
        LocalContainerEntityManagerFactoryBean emBean = new LocalContainerEntityManagerFactoryBean()
        emBean.setDataSource(dataSource)
        emBean.setPackagesToScan("com.system.bank.accounts.core")
        emBean.setJpaProperties(properties)
        emBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter())
        emBean
    }

    @Bean
    Properties properties() {
        Properties properties = new Properties()
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop")
        properties.setProperty("hibernate.show_sql", "true")
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
        properties
    }
}
